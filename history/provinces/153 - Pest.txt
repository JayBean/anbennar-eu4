#153 - Pest | Exwes

owner = A40
controller = A40
add_core = A40
culture = exwesser
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 5

trade_goods = grain
capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
