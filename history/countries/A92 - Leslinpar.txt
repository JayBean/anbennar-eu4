government = feudal_monarchy
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
capital = 901 # Leslinpar
national_focus = DIP

1440.1.12 = {
	monarch = {
		name = "Brandon III"
		dynasty = "s�l Leslinp�r"
		birth_date = 1410.3.10
		adm = 4
		dip = 3
		mil = 1
	}
}