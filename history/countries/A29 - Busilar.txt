government = feudal_monarchy
government_rank = 2
primary_culture = busilari
religion = regent_court
technology_group = tech_cannorian
capital = 408
national_focus = DIP

1367.6.12 = {
	monarch = {
		name = "Marcan I"
		dynasty = "Silnara"
		birth_date = 1366.2.3
		adm = 2
		dip = 3
		mil = 0
	}
}

1422.1.1 = { set_country_flag = lilac_wars_rose_party }