# For specific combinations of culture, religion and other such triggers
# Will pick the first valid one it finds in list

#Heir and consort titles are generally kept simple for clarity unless there is something special in particular that can be used.

adventurer_government = {
	rank = {
		1 = ADVENTURER
	}
	
	ruler_male = {
		1 = CAPTAIN
	}
	
	ruler_female = {
		1 = CAPTAIN
	}

	heir_male = {
		1 = HEIR
	}

	heir_female = {
		1 = HEIR
	}
	trigger = {
		government = adventurer
	}
}

knightly_order = {
	rank = {
		1 = KNIGHTLY_ORDER
	}

	ruler_male = {
		1 = GRANDMASTER
	}

	ruler_female = {
		1 = GRANDMASTER
	}

	trigger = {
		OR = {
			government = knightly_order
			government = monastic_order_government
		}	
	}
}

magisterium = {
	rank = {
		2 = MAGISTERIUM
	}
	
	ruler_male = {
		2 = GRAND_MAGISTER
	}
	
	ruler_female = {
		2 = GRAND_MAGISTRIX
	}

	heir_male = {
		2 = MAGISTER
	}

	heir_female = {
		2 = MAGISTRIX
	}
	trigger = {
		government = magisterium
	}
}

elven_elector_any = {
	rank = {
		1 = PRINCIPALITY
		2 = GRAND_PRINCIPALITY
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = PRINCE_ELECTOR
		2 = PRINCE_ELECTOR
		3 = EMPEROR
	}
	
	ruler_female = {
		1 = PRINCESS_ELECTOR
		2 = GRAND_ELECTRESS
		3 = EMPRESS
	}
	
	consort_male = {
		1 = PRINCE_CONSORT
		2 = PRINCE_CONSORT
		3 = PRINCE_CONSORT
	}

	consort_female = {
		1 = CONSORT
		2 = GRAND_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = PRINCE_APPARENT
		2 = PRINCE_APPARENT
		3 = HEIR_APPARENT
	}

	heir_female = {
		1 = PRINCESS_APPARENT
		2 = PRINCESS_APPARENT
		3 = HEIR_APPARENT
	}
	trigger = {
		#government = monarchy
		culture_group = elven
		is_elector = yes
		is_emperor = no
	}
}

dwarven_elector_monarchy = {
	rank = {
		1 = HOLD
		2 = GREAT_HOLD
		3 = HIGH_KINGDOM
	}
	
	ruler_male = {
		1 = LORD_ELECTOR
		2 = LORD_ELECTOR
		3 = HIGH_KING
	}

	ruler_female = {
		1 = LADY
		2 = QUEEN
		3 = HIGH_QUEEN
	}
	consort_male  = {
		1 = PRINCE_CONSORT 
		2 = KING_CONSORT
		3 = KING_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = QUEEN_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = HEIR
		3 = PRINCE
	}
	trigger = {
		government = monarchy
		culture_group = dwarven
		is_elector = yes
		is_emperor = no
	}
}

anbennar_elector_monarchy = {
	rank = {
		1 = DUCHY
		2 = GRAND_DUCHY
		3 = EMPIRE
	}

	ruler_male = {
		1 = DUKE_ELECTOR
		2 = GRAND_ELECTOR
		3 = EMPEROR
	}

	ruler_female = {
		1 = DUCHESS_ELECTOR
		2 = GRAND_ELECTRESS
		3 = EMPRESS
	}
	
	consort_male  = {
		1 = ELECTOR
		2 = ELECTOR
		3 = ELECTOR
	}
	
	consort_female = {
		1 = ELECTRESS
		2 = ELECTRESS
		3 = ELECTRESS
	}
	
	#Standard titles for heirs.
	
	trigger = {
	government = monarchy
	is_elector = yes
	is_emperor = no
	}
}

anbennar_elector_republic = {
	rank = {
		1 = REPUBLIC
		2 = GRAND_REPUBLIC
		3 = GREAT_REPUBLIC
	}
	
	ruler_male = {
		1 = LORD_ELECTOR
		2 = LORD_ELECTOR
		3 = PRESIDENT
	}

	ruler_female = {
		1 = LORD_ELECTOR
		2 = LORD_ELECTOR
		3 = PRESIDENT
	}
	trigger = {
		OR = {
			culture_group = anbennarian
			culture_group = alenic
			culture_group = halfling
			culture_group = lencori			
		}
		government = republic
		is_elector = yes
		is_emperor = no
	}
}

anbennar_elector_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = PRIEST_ELECTOR
		1 = PRIEST_ELECTOR
		1 = PRIEST_ELECTOR
	}

	ruler_female = {
		1 = PRIEST_ELECTOR
		1 = PRIEST_ELECTOR
		1 = PRIEST_ELECTOR
	}
	
	#Standard titles for heirs.
	
	trigger = {
	government = theocracy
	is_elector = yes
	is_emperor = no
	}
}

#NEEDS TESTING AS WELL AS OTHER MARCHES
march_anbennar_monarchy = {
	rank = {
		1 = MARCH
		2 = GRAND_MARCH
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = MARQUIS
		2 = GRAND_MARQUIS
		3 = EMPEROR
	}

	ruler_female = {
		1 = MARQUISE
		2 = GRAND_MARQUISE	
		3 = EMPRESS
	}
	
	consort_male = {
		1 = MARQUIS_CONSORT
		2 = MARQUIS_CONSORT
		3 = EMPEROR_CONSORT
	}

	consort_female = {
		1 = MARQUISE
		2 = MARQUISE
		3 = EMPRESS
	}

	trigger = {
		is_part_of_hre = yes
		government = monarchy
	OR = {	#You're either an actual March or a historical march of Anbennar
		is_march = yes
		tag = A72 #Arannen, march of the East
		tag = A46 #Arbaran, march of the North
		tag = A04 #Wesdam, march of the West
		}
	}
}

march_alenic_monarchy = {
	rank = {
		1 = MARCH
		2 = GREAT_MARCH
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = MARCHER_LORD
		2 = MARCHER_LORD
		3 = EMPEROR
	}

	ruler_female = {
		1 = MARCHER_LORD
		2 = MARCHER_LORD	
		3 = EMPRESS
	}
	
	consort_male  = {
		1 = CONSORT
		2 = CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = CONSORT
		3 = EMPRESS_CONSORT
	}
	trigger = {
		government = monarchy
		culture_group = alenic
		is_march = yes
	}
}

moorman_monarchy = {
	rank = {
		1 = LORDSHIP
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = LORD
		2 = MOOR_KING
		3 = MOOR_EMPEROR
	}

	ruler_female = {
		1 = LADY
		2 = MOOR_QUEEN
		3 = MOOR_EMPRESS
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		primary_culture = moorman
		government = monarchy
		is_vassal = no
	}
}

gnollish_viakkoc = {
	rank = {
		1 = PACK
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = PACK_LORD
		2 = CORSAIR_KING
		3 = SEA_SCOURGE
	}

	ruler_female = {
		1 = PACK_MISTRESS
		2 = CORSAIR_QUEEN
		3 = SEA_SCOURGE
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		culture_group = gnollish
		tag = A50
	}
}

gnollish_monarchy = {
	rank = {
		1 = PACK
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = PACK_LORD
		2 = KING
		3 = EMPEROR
	}

	ruler_female = {
		1 = PACK_MISTRESS
		2 = QUEEN
		3 = EMPRESS
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		culture_group = gnollish
		government = steppe_horde
	}
}

cannorian_minaran_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = EXALTED_COMPANION
		2 = EXALTED_COMPANION
		3 = EXALTED_COMPANION
	}

	ruler_female = {
		1 = EXALTED_COMPANION
		2 = EXALTED_COMPANION
		3 = EXALTED_COMPANION
	}

	trigger = {
		government = theocracy
		tag = A10
	}
}

elven_principality_republic = {	#changed it to all elves
	rank = {
		1 = PRINCIPALITY
		2 = GRAND_PRINCIPALITY
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = PRINCE
		2 = GRAND_PRINCE
		3 = EMPEROR
	}
	
	ruler_female = {
		1 = PRINCESS
		2 = GRAND_PRINCESS
		3 = EMPRESS
	}
	
	consort_male = {
		1 = PRINCE_CONSORT
		2 = PRINCE_CONSORT
		3 = PRINCE_CONSORT
	}

	consort_female = {
		1 = CONSORT
		2 = GRAND_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = PRINCE_APPARENT
		2 = PRINCE_APPARENT
		3 = HEIR_APPARENT
	}

	heir_female = {
		1 = PRINCESS_APPARENT
		2 = PRINCESS_APPARENT
		3 = HEIR_APPARENT
	}
	
	trigger = {
		culture_group = elven
		#government = elven_principality
	}
}

anbennar_monarchy_county = {	#For special counts
	rank = {
		1 = COUNTY
	}
	
	ruler_male = {
		1 = COUNT
	}

	ruler_female = {
		1 = COUNTESS
	}
	consort_male  = {
		1 = CONSORT 
	}
	
	consort_female = {
		1 = CONSORT
	}
	
	heir_male = {
		1 = HEIR
	}
	
	heir_female = {
		1 = HEIR
	}
	
	trigger = {
	OR = {
		culture_group = anbennarian
		culture_group = lencori
	}
	government = monarchy
	has_country_flag = is_a_county
	}
}

anbennar_monarchy = {	
	rank = {
		1 = DUCHY
		2 = GRAND_DUCHY
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = DUKE
		2 = GRAND_DUKE
		3 = EMPEROR
	}

	ruler_female = {
		1 = DUCHESS
		2 = GRAND_DUCHESS	
		3 = EMPRESS
	}
	consort_male  = {
		1 = CONSORT 
		2 = GRAND_DUKE_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = GRAND_DUKE_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = HEIR	
		3 = PRINCESS
	}
	
	trigger = {
	government = monarchy
	is_part_of_hre = yes
	OR = {
		culture_group = anbennarian
		culture_group = lencori
		culture_group = alenic
		}
	}
}

anbennar_republic = {
	rank = {
		1 = REPUBLIC
		2 = GRAND_REPUBLIC
		3 = GREAT_REPUBLIC
	}
	
	ruler_male = {
		1 = LORD_MAYOR
		2 = GRAND_MAYOR
		3 = PRESIDENT
	}

	ruler_female = {
		1 = LORD_MAYOR
		2 = GRAND_MAYOR	
		3 = PRESIDENT
	}
	trigger = {
		OR = {
			culture_group = anbennarian
			culture_group = alenic
			culture_group = halfling
			culture_group = lencori			
		}
		government = republic
	}
}

cannorian_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = HIGH_PRIEST
		2 = HIGH_PRIEST
		3 = HIGH_PRIEST
	}

	ruler_female = {
		1 = HIGH_PRIESTESS
		2 = HIGH_PRIESTESS
		3 = HIGH_PRIESTESS
	}

	trigger = {
		government = theocracy
		religion_group = cannorian
	}
}

lencori_monarchy = {	
	rank = {
		1 = DUCHY
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = DUKE
		2 = KING
		3 = EMPEROR
	}

	ruler_female = {
		1 = DUCHESS
		2 = QUEEN	
		3 = EMPRESS
	}
	consort_male  = {
		1 = CONSORT 
		2 = PRINCE_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
	government = monarchy
	culture_group = lencori
	}
}

alenic_monarchy = {
	rank = {
		1 = LORDSHIP
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = LORD
		2 = KING
		3 = EMPEROR
	}

	ruler_female = {
		1 = LADY
		2 = QUEEN
		3 = EMPRESS
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		culture_group = alenic
		government = monarchy
	}
}

gnomish_republic = {
	rank = {
		1 = HIERARCHY
		2 = HIERARCHY
		3 = SUPREME_HIERARCHY
	}
	
	ruler_male = {
		1 = HIERARCH
		2 = HIERARCH
		3 = SUPREME_HIERARCH
	}

	ruler_female = {
		1 = HIERARCH
		2 = HIERARCH	
		3 = SUPREME_HIERARCH
	}
	trigger = {
		culture_group = gnomish
		government = republic
	}
}

gnomish_kingdom = {
	rank = {
		1 = HIERARCHY
		2 = HIERARCHY
		3 = SUPREME_HIERARCHY
	}
	
	ruler_male = {
		1 = HIERARCH_MONARCHY
		2 = HIERARCH_MONARCHY
		3 = SUPREME_HIERARCH_MONARCHY
	}

	ruler_female = {
		1 = HIERARCH_MONARCHY
		2 = HIERARCH_MONARCHY
		3 = SUPREME_HIERARCH_MONARCHY
	}
	trigger = {
		NOT = { tag = A80 }	#Iochand is a Kingdom proper
		culture_group = gnomish
		government = monarchy
	}
}

dwarven_monarchy = {
	rank = {
		1 = HOLD
		2 = KINGDOM
		3 = HIGH_KINGDOM
	}
	
	ruler_male = {
		1 = LORD
		2 = KING
		3 = HIGH_KING
	}

	ruler_female = {
		1 = LADY
		2 = QUEEN
		3 = HIGH_QUEEN
	}
	consort_male  = {
		1 = PRINCE_CONSORT 
		2 = KING_CONSORT
		3 = KING_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = QUEEN_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	trigger = {
		culture_group = dwarven
		government = monarchy
	}
}
