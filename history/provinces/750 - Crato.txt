# No previous file for Crato
owner = B03
controller = B03
add_core = B03
culture = gawedi
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = fur

capital = ""

is_city = yes

native_size = 22
native_ferocity = 8
native_hostileness = 8

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish