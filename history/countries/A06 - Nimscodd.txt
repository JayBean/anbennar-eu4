government = oligarchic_republic
government_rank = 1
primary_culture = cliff_gnome
religion = the_thought
technology_group = tech_gnomish
capital = 169 #Nimscodd
national_focus = DIP

1390.6.11 = {
	monarch = {
		name = "Mordibam"
		dynasty = "Quintain"
		birth_date = 1388.5.3
		adm = 6
		dip = 6
		mil = 6
	}
	add_ruler_personality = careful_personality
}